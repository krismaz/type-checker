module Substitution where
  import Types
  import qualified Data.List as List

  type Substitution = [(String, Term)]

  subLookup :: Substitution -> String -> Term
  subLookup phi n = case List.find ((n ==) . fst) phi of
    Just (name, t) -> t
    Nothing -> TVar n

  subExclude :: Substitution -> [String] -> Substitution
  subExclude phi ns = filter (\(name, t) -> not (elem name ns)) phi

  subType :: Substitution -> Term -> Term
  subType phi (TVar name) = subLookup phi name
  subType phi (TFun name lst) = TFun name (map (subType phi) lst)

  subScheme :: Substitution -> Scheme -> Scheme
  subScheme phi (Scheme ns t) =
    let phi' = subExclude phi ns in Scheme ns (subType phi' t)

  subTypeEnv :: Substitution -> TypeEnv -> TypeEnv
  subTypeEnv phi = map (\(name, sch) -> (name, subScheme phi sch))

  subCompose :: Substitution -> Substitution -> Substitution
  subCompose phi2 phi1 =
    let overlap = map (\(name, t) -> (name, subType phi2 t)) phi1;
        getKeys = map fst;
        [keys1, keys2] = map getKeys [phi1, phi2];
        keysRest = filter (\n -> not (elem n keys1)) keys2;
        rest = map (\n -> (n, subLookup phi2 n)) keysRest;
    in
      overlap ++ rest

  subExpand :: Substitution -> Substitution
  subExpand = foldr (\e acc -> subCompose acc [e]) []
