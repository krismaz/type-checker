module Parser where
  import Stream
  import Types
  import qualified Data.Char as Char
  import qualified Data.List as List
  import Control.Applicative
  import Control.Monad

  type CharStream = Stream Char
  data Parser a = Parser (CharStream -> Stream (a, CharStream))

  unwrap (Parser f) = f

  instance Monad Parser where
    return x = Parser (\str -> Stream.sunit (x, str))

    Parser ps >>= f =
      Parser (Stream.sconcat . (Stream.smap $ uncurry $ unwrap . f) . ps)

  instance MonadPlus Parser where
    mzero = Parser (\str -> Stream.snil)
    
    mplus (Parser p1) (Parser p2) =
      Parser (\str -> Stream.sconcat2 (p1 str) (p2 str))

  instance Functor Parser where
    fmap = liftM

  instance Applicative Parser where
    pure = return
    (<*>) = ap

  instance Alternative Parser where
    empty = mzero
    (<|>) = mplus

  (<<) :: Parser a -> Parser b -> Parser a
  p1 << p2 = p1 >>= (\x -> p2 >> return x)

  plimit :: Parser a -> Parser a
  plimit (Parser p) = Parser (Stream.limit . p)

  pfilter :: (a -> Bool) -> Parser a -> Parser a
  pfilter pred (Parser p) = Parser (Stream.sfilter (pred . fst) . p)

  psat :: (Char -> Bool) -> Parser Char
  psat pred =
    let fun e = List.find (pred . fst) [e] in
      Parser (\(Stream str) -> Stream.fromMaybe $ str >>= fun)

  pchar :: Char -> Parser Char
  pchar c = psat (== c)

  papp :: Parser a -> Parser [a] -> Parser [a]
  papp p plst = p >>= (\e -> (e :) <$> plst)

  plist :: [Parser a] -> Parser [a]
  plist = foldr papp $ return []

  pstring :: String -> Parser String
  pstring = plist . map pchar

  pmany :: Parser a -> Parser [a]
  pmany p = pmany1 p <|> return []

  pmany1 :: Parser a -> Parser [a]
  pmany1 p = papp p $ pmany p

  palpha = psat Char.isAlpha
  palphanum = psat Char.isAlphaNum
  pdigit = psat Char.isDigit
  pspace = psat Char.isSpace

  pspaces = pmany pspace

  pignorespaces :: Parser a -> Parser a
  pignorespaces p = pspaces >> p << pspaces

  pbrackets :: Parser a -> Parser a
  pbrackets p = pchar '(' >> p << pchar ')'

  pint :: Parser LTerm
  pint = Number <$> read <$> pmany1 pdigit

  psep :: Parser a -> Parser b -> Parser [b]
  psep sep p = papp p . pmany $ sep >> p

  notSpecial :: String -> Bool
  notSpecial s = not (elem s ["fn", "let", "in", "end", "val", "fun"])

  plabelstr :: Parser String
  plabelstr
    = pignorespaces
    . pfilter notSpecial
    . plimit
    . papp palpha
    $ pmany palphanum

  plabel :: Parser LTerm
  plabel = Label <$> plabelstr

  ptermstr :: String -> Parser String
  ptermstr = pignorespaces . pstring

  pcons = ptermstr "::"
  pplus = ptermstr "+"
  pfn = ptermstr "fn"
  parrow = ptermstr "=>"
  plet = ptermstr "let"
  pin = ptermstr "in"
  pend = ptermstr "end"
  pfun = ptermstr "fun"
  peq = ptermstr "="
  pval = ptermstr "val"

  makeConsRev :: LTerm -> LTerm -> LTerm
  makeConsRev a b = App (App (Label "CONS") b) a

  makePlus :: LTerm -> LTerm -> LTerm
  makePlus a b = App (App (Label "PLUS") a) b

  pterm :: Parser LTerm
  pterm = foldl1 App <$> pmany1 ptermNoApp

  ptermNoApp :: Parser LTerm
  ptermNoApp = foldl1 makeConsRev . List.reverse <$> psep pcons ptermNoCons

  ptermNoCons :: Parser LTerm
  ptermNoCons = foldl1 makePlus <$> psep pplus ptermNoAdd

  ptermWithHead :: a -> Parser (a, LTerm)
  ptermWithHead = \head -> ((,) head) <$> pterm

  pdefVal :: Parser (String, LTerm)
  pdefVal = pval >> pspace >> plabelstr << peq >>= ptermWithHead

  pdefFun :: Parser (String, LTerm)
  pdefFun =
    let tail = pmany1 plabelstr << peq >>= ptermWithHead;
        glue name (args, body) = (name, List.foldr Abs body args);
    in
      pfun >> pspace >> plabelstr >>= \name -> glue name <$> tail

  ptermFn :: Parser LTerm
  ptermFn =
    pfn >> pspace >> plabelstr << parrow >>= \name -> Abs name <$> pterm

  ptermLet :: Parser LTerm
  ptermLet =
    let defs = pmany (pdefVal <|> pdefFun) in
      (plet >> pspace >> defs << pin >>= (\df -> Let df <$> pterm)) << pend

  ptermNoAdd :: Parser LTerm
  ptermNoAdd
    = plimit
    . pignorespaces
    $ ptermFn <|> ptermLet <|> pbrackets pterm <|> pint <|> plabel

  parse :: String -> Maybe LTerm
  parse s = fst <$> (Stream.shd . unwrap pterm . Stream.ofList $ s)
