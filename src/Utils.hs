module Utils where
  mapWithDs :: (d -> a -> (d, b)) -> d -> [a] -> (d, [b])
  mapWithDs fn ds =
    let combine x (ds, lst) = (ds', y : lst) where (ds', y) = fn ds x;
        init = (ds, []);
    in
      foldr combine init

  allTrue :: [Bool] -> Bool
  allTrue = foldr (&&) True
