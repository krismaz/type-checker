module Typechecker where
  import Stream
  import Substitution
  import Types
  import Unify
  import Utils
  import qualified Control.Arrow as Arrow
  import qualified Data.List as List
  import qualified Data.Maybe as Maybe

  type NameSupply = Stream String

  typeEnvLookup :: TypeEnv -> String -> Scheme
  typeEnvLookup env n = snd . Maybe.fromJust . List.find ((n ==) . fst) $ env

  varsIn :: Term -> [String]
  varsIn (TVar n) = [n]
  varsIn (TFun _ lst) = concatMap varsIn lst

  varsInDedup :: Term -> [String]
  varsInDedup = List.nub . varsIn

  unknownsScheme :: Scheme -> [String]
  unknownsScheme (Scheme vs t) = filter (not . flip elem vs) $ varsIn t

  unknownsTypeEnv :: TypeEnv -> [String]
  unknownsTypeEnv = concatMap (unknownsScheme . snd)

  makeArrow :: Term -> Term -> Term
  makeArrow x y = TFun "arrow" [x, y]

  typeToTerm :: Type -> Term
  typeToTerm (Var n) = TVar n
  typeToTerm (Arr t1 t2) = makeArrow (typeToTerm t1) (typeToTerm t2)
  typeToTerm (List t) = TFun "list" [typeToTerm t]
  typeToTerm Int = TFun "int" []

  termToType :: Term -> Type
  termToType (TVar n) = Var n
  termToType (TFun n lst) =
    case (n, map termToType lst) of
      ("arrow", [t1, t2]) -> Arr t1 t2
      ("list", [t]) -> List t
      ("int", []) -> Int
      _ -> error "Incorrect term given as argument to termToType"

  zipWithFreshVars :: NameSupply -> [a] -> (NameSupply, [(a, Term)])
  zipWithFreshVars ns =
    foldr (\x (Stream (Just (hd, tl)), lst) -> (tl, (x, TVar hd) : lst)) (ns, [])

  varName :: Term -> String
  varName (TVar n) = n
  varName (TFun _ _) = error "Cannot apply varName to TFun _ _"

  genBar :: [String] -> NameSupply -> Term -> (NameSupply, (Substitution, Scheme))
  genBar unknowns ns t
    = Arrow.second (\(sub, ns, t) -> (sub, Scheme ns t))
    . Arrow.second (\sub -> (sub, map (varName . snd) sub, subType sub t))
    . zipWithFreshVars ns
    . filter (not . flip elem unknowns)
    . varsInDedup
    $ t;

  addDecls :: TypeEnv -> NameSupply -> [String] -> [Term] -> (NameSupply, (Substitution, TypeEnv))
  addDecls env ns xs
    = Arrow.second (Arrow.second (++ env))
    . Arrow.second (\pairs -> (concatMap fst pairs, List.zip xs . map snd $ pairs))
    . mapWithDs (genBar . unknownsTypeEnv $ env) ns

  tcheckRaw :: TypeEnv -> NameSupply -> LTerm -> Maybe (NameSupply, Substitution, Term)
  tcheckRaw _ ns (Number _) = Just (ns, [], typeToTerm Int)
  tcheckRaw env ns (Label n)
    = Just
    . (\((ns, sub), t) -> (ns, [], subType sub t))
    . (\(Scheme vs t) -> (zipWithFreshVars ns vs, t))
    . typeEnvLookup env
    $ n
  tcheckRaw env ns (App a b) =
    tcheckListRaw env ns [a, b]
    >>= (\(ns, sub, lst) -> Just (Stream.seval ns, sub, lst))
    >>= (\((n, ns), sub, lst) -> Just (n, ns, Unify.unify sub, lst))
    >>= (\(n, ns, uni, [t1, t2]) -> ((,,) n ns) <$> uni t1 (makeArrow t2 (TVar n)))
    >>= (\(n, ns, sub) -> Just (ns, sub, subLookup sub n))
  tcheckRaw env (Stream s) (Abs x e) =
    s
    >>= (\(n, ns) -> Just (n, ns, Scheme [] (TVar n)))
    >>= (\(n, ns, sch) -> Just (n, ns, (x, sch) : env))
    >>= (\(n, ns, env) -> ((,) n) <$> tcheckRaw env ns e)
    >>= (\(n, (ns, sub, t)) -> Just (ns, sub, makeArrow (subLookup sub n) t))
  tcheckRaw env ns (Let defs e) =
    let names = map fst defs;
        rhSides = map snd defs;
        (ns', lhEnv)
          = (Arrow.second . map . Arrow.second . Scheme $ [])
          . zipWithFreshVars ns
          $ names;
        lhsVars = map (\(_, Scheme _ v) -> v) lhEnv;
        env' = lhEnv ++ env;
    in
      tcheckListRaw env' ns' rhSides
      >>= (\(ns, sub, ts) -> Just (ns, sub, map (subType sub) lhsVars, ts))
      >>= (\(ns, sub, ts', ts) -> ((,,) ns ts) <$> Unify.lunify sub ts' ts)
      >>= (\(ns, ts, sub) -> Just (ns, map (subType sub) ts, sub))
      >>= (\(ns, ts, sub) -> Just (sub, addDecls (subTypeEnv sub env) ns names ts))
      >>= (\(subOld, (ns, (subNew, env))) -> Just (ns, subCompose subNew subOld, env))
      >>= (\(ns, sub, env) -> ((,) sub) <$> tcheckRaw (subTypeEnv sub env) ns e)
      >>= (\(subOld, (ns, subNew, t)) -> Just (ns, subCompose subNew subOld, t))

  tcheckListRaw :: TypeEnv -> NameSupply -> [LTerm] -> Maybe (NameSupply, Substitution, [Term])
  tcheckListRaw _ ns [] = Just (ns, [], [])
  tcheckListRaw env ns (e : es) =
    tcheckRaw env ns e
    >>= (\(ns, sub, t) -> ((,,) sub t) <$> (tcheckListRaw (subTypeEnv sub env) ns es))
    >>= (\(sub, t, (ns, sub', ts)) -> Just (ns, subCompose sub' sub, (subType sub' t) : ts))

  makeTypeEnv :: [(String, Type)] -> TypeEnv
  makeTypeEnv
    = map (Arrow.second (\t -> Scheme (varsInDedup t) t))
    . map (Arrow.second typeToTerm)

  nameSupply :: NameSupply
  nameSupply =
    let intStream n = Stream . Just $ (n, intStream . succ $ n) in
        Stream.smap ("v" ++)
      . Stream.smap show
      $ intStream 0

  typeCheck :: [(String, Type)] -> LTerm -> Maybe Type
  typeCheck context t =
    (\(_, _, t) -> termToType t) <$> (tcheckRaw (makeTypeEnv context) nameSupply t)
