module Unify where
  import Types
  import Stream
  import Substitution
  import Utils
  import qualified Control.Arrow as Arrow
  import qualified Data.Map.Strict as Map

  data TermGen = VarGen String
               | FunGen String [Integer]
    deriving Show

  type IntMap a = Map.Map Integer a
  type StringMap a = Map.Map String a

  data DSU = DSU {
    element :: IntMap TermGen
  , parent :: IntMap Integer
  , scheme :: IntMap Integer
  , visited :: IntMap Bool
  , acyclic :: IntMap Bool
  , vars :: IntMap (Stream String)
  } deriving Show

  dsuEmpty :: DSU
  dsuEmpty = DSU Map.empty Map.empty Map.empty Map.empty Map.empty Map.empty

  getName :: TermGen -> String
  getName (FunGen s _) = s
  getName (VarGen s) = s

  get :: (DSU -> IntMap a) -> DSU -> Integer -> a
  get field dsu x = (field dsu) Map.! x

  unionScheme :: DSU -> Integer -> Integer -> Integer
  unionScheme dsu x y = case get element dsu x of
    VarGen _ -> y
    FunGen _ _ -> x

  find :: DSU -> Integer -> (DSU, Integer)
  find dsu x = case Map.lookup x (parent dsu) of
    Nothing -> (dsu, x)
    Just y -> (dsu', root)
      where ((DSU _ parent' _ _ _ _), root) = find dsu y
            dsu' = dsu { parent = Map.insert x root parent' }

  union :: DSU -> Integer -> Integer -> DSU
  union dsu x y =
    let (dsu', xRoot) = find dsu x;
        (dsu'', yRoot) = find dsu' y;
    in
      if xRoot == yRoot then
        dsu''
      else
        let parent' = Map.insert yRoot xRoot (parent dsu'');
            [xScheme, yScheme] = map (get scheme dsu'') [xRoot, yRoot];
            scheme' = Map.insert xRoot (unionScheme dsu'' xScheme yScheme) (scheme dsu'');
            [xVars, yVars] = map (get vars dsu'') [xRoot, yRoot];
            vars' = Map.insert xRoot (Stream.sconcat2 xVars yVars) (vars dsu'');
        in
          dsu'' { parent = parent', scheme = scheme', vars = vars' }

  makeTerm :: DSU -> Integer -> Term
  makeTerm dsu x = case get element dsu x of
    VarGen name -> TVar name
    FunGen name lst -> TFun name (map (makeTerm dsu) lst)

  createNewNode :: DSU -> TermGen -> (DSU, Integer)
  createNewNode (DSU element parent scheme visited acyclic vars) t =
    let x = toInteger (Map.size element);
        element' = Map.insert x t element;
        scheme' = Map.insert x x scheme;
        visited' = Map.insert x False visited;
        acyclic' = Map.insert x False acyclic;
        tVars = case t of
          VarGen name -> Stream.sunit name
          FunGen _ _ -> Stream.snil;
        vars' = Map.insert x tVars vars;
    in
      (DSU element' parent scheme' visited' acyclic' vars', x)

  makeNodes :: (DSU, StringMap Integer) -> Term -> ((DSU, StringMap Integer), Integer)
  makeNodes ds t = case t of
    TVar name ->
      case Map.lookup name (snd ds) of
        Just x -> (ds, x)
        Nothing ->
            (\((dsu, x), dict) -> ((dsu, Map.insert name x dict), x))
          . Arrow.first (\dsu -> createNewNode dsu (VarGen name))
          $ ds
    TFun name lst ->
        (\(dict, (dsu, x)) -> ((dsu, dict), x))
      . (\((dsu, dict), e) -> (dict, createNewNode dsu e))
      . Arrow.second (FunGen name)
      . mapWithDs makeNodes ds
      $ lst

  unifyClosure :: DSU -> Integer -> Integer -> (DSU, Bool)
  unifyClosure dsu x y =
    let (dsu', [xRoot, yRoot]) = mapWithDs find dsu [x, y] in
      if xRoot == yRoot then
        (dsu', True)
      else
        let [xScheme, yScheme] = map (get scheme dsu') [xRoot, yRoot];
            [xElem, yElem] = map (get element dsu') [xScheme, yScheme];
        in
          case (xElem, yElem) of
            (FunGen xName xLst, FunGen yName yLst) ->
              if xName /= yName then
                (dsu', False)
              else
                let dsu'' = union dsu' xScheme yScheme;
                    zipUnify dsu [] [] = (dsu, True);
                    zipUnify dsu (hd1 : tl1) (hd2 : tl2) =
                      let (dsu', hdResult) = unifyClosure dsu hd1 hd2;
                          (dsu'', tlResult) = zipUnify dsu' tl1 tl2;
                      in
                        (dsu'', hdResult && tlResult)
                    zipUnify dsu _ _ = (dsu, False);
                in
                  zipUnify dsu'' xLst yLst
            _ ->
              (union dsu' xScheme yScheme, True)

  findSolution :: (DSU, Substitution) -> Integer -> ((DSU, Substitution), Bool)
  findSolution (dsu, sub) x =
    let (dsu', xRoot) = find dsu x;
        sch = get scheme dsu' xRoot;
    in
      if get acyclic dsu' sch then
        ((dsu', sub), True)
      else if get visited dsu' sch then
        ((dsu', sub), False)
      else
        let myName = getName (get element dsu' sch);
            setVisited val dsu = dsu { visited = Map.insert sch val (visited dsu) };
            (ds, res) = case get element dsu' sch of
              VarGen _ -> ((dsu', sub), True)
              FunGen _ lst ->
                  Arrow.first (Arrow.first $ setVisited False)
                . Arrow.second allTrue
                . (\e -> mapWithDs findSolution e lst)
                . Arrow.first (setVisited True)
                $ (dsu', sub)
        in
          let dsRes
                = (\((dsu, addToSub), sub) -> (dsu, addToSub ++ sub))
                . Arrow.first (\(dsu, vs) -> (dsu, map (\name -> (name, makeTerm dsu sch)) vs))
                . Arrow.first (Arrow.second $ (Stream.toList . Stream.sfilter (myName /=)))
                . Arrow.first (\(dsu, schRoot) -> (dsu, get vars dsu schRoot))
                . Arrow.first (\dsu -> find dsu sch)
                . Arrow.first (\dsu -> dsu { acyclic = Map.insert sch True (acyclic dsu) })
                $ ds;
          in
            (dsRes, res)

  unifyRaw :: Term -> Term -> Maybe Substitution
  unifyRaw t1 t2 =
    let (x, (dsu, res))
          = (\(dsu, [x1, x2]) -> (x1, unifyClosure dsu x1 x2))
          . Arrow.first fst
          . mapWithDs makeNodes (dsuEmpty, Map.empty)
          $ [t1, t2];
    in
      if not res then
        Nothing
      else
        let ((_, sub), res) = findSolution (dsu, []) x in
          if res then
            Just sub
          else
            Nothing

  lunifyRaw :: [Term] -> [Term] -> Maybe Substitution
  lunifyRaw lst1 lst2 = concat <$> (sequence . map (uncurry unifyRaw)) (zip lst1 lst2)

  unify :: Substitution -> Term -> Term -> Maybe Substitution
  unify phi t1 t2 = ((\s -> subCompose s phi) . subExpand) <$> (unifyRaw t1 t2)

  lunify :: Substitution -> [Term] -> [Term] -> Maybe Substitution
  lunify phi lst1 lst2 = ((\s -> subCompose s phi) . subExpand) <$> (lunifyRaw lst1 lst2)
