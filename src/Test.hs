module Test where
  import qualified Data.List as List
  import Parser
  import Typechecker
  import Types
  import Utils

  infixr 6 -->
  a --> b = Arr a b

  defaultContext :: [(String, Type)]
  defaultContext = [
    ("PLUS", Int --> Int --> Int),
    ("CONS", Var "a" --> (List (Var "a")) --> (List (Var "a"))),
    ("NIL", List (Var "a")),
    ("hd", (List (Var "a")) --> Var "a"),
    ("IF", Int --> (Var "a") --> (Var "a") --> (Var "a")),
    ("tl", (List (Var "a")) --> (List (Var "a")))]

  typesEq :: Maybe Type -> Maybe Type -> Bool
  typesEq Nothing Nothing = True
  typesEq (Just t1) (Just t2) =
    let typesEqRec t1 t2 mapping =
          case (t1, t2) of
            (Var n1, Var n2) -> (True, (n1, n2) : mapping)
            (Arr a1 b1, Arr a2 b2) ->
              let (aRes, mapping') = typesEqRec a1 a2 mapping;
                  (bRes, mapping'') = typesEqRec b1 b2 mapping';
              in
                (aRes && bRes, mapping'')
            (List a1, List a2) -> typesEqRec a1 a2 mapping
            (Int, Int) -> (True, mapping)
            _ -> (False, mapping);
        (res, mapping) = typesEqRec t1 t2 [];
    in
      if res then
        let mapping' = List.nub mapping;
            lstNoRepeats lst = (length . List.nub $ lst) == (length lst);
        in
          (lstNoRepeats (map fst mapping')) && (lstNoRepeats (map snd mapping'))
      else
        False
  typesEq _ _ = False

  tests :: [String]
  tests = [
    "(fn a => fn b => fn c => (a c) (b c))",

    "(fn S => fn K1 => fn K2 => S K1 K2)\n\
    \(fn a => fn b => fn c => (a c) (b c))\n\
    \(fn x=> fn y => x)\n\
    \(fn x=> fn y => x)",

    "(fn S => fn K => S K K)\n\
    \(fn a => fn b => fn c => (a c) (b c))\n\
    \(fn x=> fn y => x)",

    "let\n\
    \   fun S a b c = a c (b c)\n\
    \   fun K a b = a\n\
    \in S K K end",

    "(fn x=> fn y => x::y)",

    "fn y => y + 1",

    "(fn I => (let\n\
    \          val x = let val y = I 7 in I x end\n\
    \          in x end)\n\
    \)(fn x => x)",

    "let\n\
    \   fun foldl f z l1 =IF 0 z (foldl f (f z (hd l1)) (tl l1))\n\
    \   val ones = 1::ones\n\
    \   fun add x y = x+y\n\
    \   fun f1 i = 1+ (f2 i)\n\
    \   fun f2 i = 1+ (f1 i)\n\
    \in  foldl add   end",

    "let\n\
    \   fun f1 x = f2 x\n\
    \   fun f2 x = f3 x\n\
    \   fun f3 x = 1 + (f1 x)\n\
    \in f3 end"]

  va = Var "a"
  vb = Var "b"
  vc = Var "c"

  answers :: [Maybe Type]
  answers = [
      Just $ (va --> vb --> vc) --> (va --> vb) --> (va --> vc),
      Just $ (va --> va),
      Nothing,
      Just $ (va --> va),
      Just $ (va --> (List va) --> (List va)),
      Just $ (Int --> Int),
      Just $ Int,
      Just $ (Int --> (List Int) --> Int),
      Just $ (va --> Int)
    ]

  parseAndTypeCheck :: String -> Maybe Type
  parseAndTypeCheck s = Parser.parse s >>= Typechecker.typeCheck defaultContext

  testResults :: Bool
  testResults
    = allTrue
    . (map . uncurry $ typesEq)
    . (List.zip answers)
    . (map parseAndTypeCheck)
    $ tests

  testTypechecker :: IO ()
  testTypechecker = do
    if testResults then
      print "Tests OK"
    else
      error "Tests failed"
