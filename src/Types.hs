module Types where
  data LTerm = Number Integer
             | Label String
             | App LTerm LTerm
             | Abs String LTerm
             | Let [(String, LTerm)] LTerm
    deriving Show

  data Type = Var String
            | Arr Type Type
            | List Type
            | Int
    deriving Show

  data Term = TFun String [Term]
            | TVar String
    deriving Show

  data Scheme = Scheme [String] Term
    deriving Show

  type TypeEnv = [(String, Scheme)]
