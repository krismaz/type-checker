import Types
import Unify
import Parser
import Typechecker
import Test


main :: IO ()
main = do
  testTypechecker
  input <- getContents
  print (Parser.parse input >>= Typechecker.typeCheck Test.defaultContext)
