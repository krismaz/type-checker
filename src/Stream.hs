module Stream where
  import qualified Data.Maybe as Maybe

  data Stream a = Stream (Maybe (a, Stream a))

  snil :: Stream a
  snil = Stream Nothing

  sunit :: a -> Stream a
  sunit x = Stream (Just (x, snil))

  shd :: Stream a -> Maybe a
  shd (Stream e) = fst <$> e

  seval :: Stream a -> (a, Stream a)
  seval (Stream s) = Maybe.fromJust s

  sconcat2 :: Stream a -> Stream a -> Stream a
  sconcat2 (Stream Nothing) s2 = s2
  sconcat2 (Stream (Just (x, tl))) s2 = Stream (Just (x, sconcat2 tl s2))

  sconcat :: Stream (Stream a) -> Stream a
  sconcat (Stream Nothing) = snil
  sconcat (Stream (Just (s, tl))) = sconcat2 s (sconcat tl)

  sadd :: a -> Stream a -> Stream a
  sadd = sconcat2 . sunit

  sfilter :: (a -> Bool) -> Stream a -> Stream a
  sfilter _ (Stream Nothing) = snil
  sfilter fun (Stream (Just (x, tl)))
    | fun x     = Stream (Just (x, sfilter fun tl))
    | otherwise = sfilter fun tl

  smap :: (a -> b) -> Stream a -> Stream b
  smap fun (Stream Nothing) = snil
  smap fun (Stream (Just (x, tl))) = Stream (Just (fun x, smap fun tl))

  ssplit :: Stream a -> (Stream a, Stream a)
  ssplit (Stream Nothing) = (snil, snil)
  ssplit (Stream (Just (x, tl))) = (sadd x res2, res1)
    where (res1, res2) = ssplit tl

  toList :: Stream a -> [a]
  toList (Stream Nothing) = []
  toList (Stream (Just (x, tl))) = x : toList tl

  ofList :: [a] -> Stream a
  ofList [] = snil
  ofList lst = foldr1 sconcat2 . map sunit $ lst

  fromMaybe :: Maybe a -> Stream a
  fromMaybe Nothing = snil
  fromMaybe (Just x) = sunit x

  limit :: Stream a -> Stream a
  limit (Stream e) = fromMaybe $ fst <$> e

  instance Show a => Show (Stream a) where
    show = show . toList
